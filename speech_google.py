import speech_recognition as sr
device_index = 12
# Record Audio
r = sr.Recognizer()
'''
with sr.Microphone(device_index=4) as source:
m = sr.Microphone()
'''
#set threhold level
with sr.Microphone(device_index) as source: r.adjust_for_ambient_noise(source)

print("Set minimum energy threshold to {}".format(r.energy_threshold))

# Speech recognition using Google Speech Recognition
def checkspeech(r):
    with sr.Microphone(device_index) as source:
        audio = r.listen(source)
    try:
    # for testing purposes, we're just using the default API key
    # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
    # instead of `r.recognize_google(audio)`
        print("You said: " + r.recognize_google(audio))
        return (r.recognize_google(audio))
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
        return ("WW")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
        return ("WW")
while 1:
	speech = str(checkspeech(r))
